fun main(args: Array<String>) {
    val author = Author(1, "Jerome", "Salinger", "USA")
    /*val note = Note(1,"Something","Make America great again", author)

    println(note)

    val test = Test()
    test.print()
*/

    val db = OurDB()
    db.addNewAuthor(author)
    val list = db.findAllAuthors()
    println(list)
}