data class Note(
       private val id: Long,
       private val title: String,
       private val description: String,
       private val author: Author
) {
    override fun toString(): String {

        return (author).toString() + " say " + title + "\nthat mean " +description
    }
}