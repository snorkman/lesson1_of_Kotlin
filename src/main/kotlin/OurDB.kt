import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement

class OurDB {
    var stmt: Statement? = null
    var rs: ResultSet? = null
    private val user = System.getenv("USER_DB")
    private val password = System.getenv("PASSWORD")
    private val path = System.getenv("PATH_DB")
    private val conn = DriverManager.getConnection("jdbc:mysql://$path?user=$user&password=$password&useSSL=true")


    fun addNewAuthor(author: Author) {
        val sql = "INSERT INTO authors (firstName,secondName,country) VALUES (\"${author.firstName}\",\"${author.secondName}\",\"${author.country}\");"
        try {
            stmt = conn.createStatement()
            stmt!!.execute(sql)

        } catch (ex: SQLException) {
            println("SQLException: " + ex.message)
            println("SQLState: " + ex.sqlState)
            println("VendorError: " + ex.errorCode)
        } finally {
            rs?.close()
            stmt?.close()
        }
    }

    fun removeAuthor(author: Author) {
        val sql = "delete from authors where id = ${author.id}"
        try {
            stmt = conn.createStatement()
            stmt!!.execute(sql)

        } catch (ex: SQLException) {
            println("SQLException: " + ex.message)
            println("SQLState: " + ex.sqlState)
            println("VendorError: " + ex.errorCode)
        } finally {
            rs?.close()
            stmt?.close()
        }
    }

    fun findAllAuthors(): List<Author> {
        val sql = "SELECT * FROM authors"
        val list = mutableListOf<Author>()
        var author: Author
        var id: Int
        var firstName: String
        var secondName: String
        var country: String
        try {
            stmt = conn.createStatement()
            rs = stmt!!.executeQuery(sql)

            while (rs!!.next()) {
                id = rs!!.getInt(1)
                firstName = rs!!.getString(2)
                secondName = rs!!.getString(3)
                country = rs!!.getString(4)
                author = Author(id, firstName, secondName, country)
                list.add(author)
            }

        } catch (ex: SQLException) {
            println("SQLException: " + ex.message)
            println("SQLState: " + ex.sqlState)
            println("VendorError: " + ex.errorCode)
        } finally {
            rs?.close()
            stmt?.close()
        }
        return list
    }

}